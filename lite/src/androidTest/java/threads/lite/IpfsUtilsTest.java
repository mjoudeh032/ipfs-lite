package threads.lite;


import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.math.BigInteger;
import java.net.UnknownHostException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;

import threads.lite.cid.Cid;
import threads.lite.cid.ID;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.Peer;
import threads.lite.cid.PeerId;
import threads.lite.dht.QueryPeer;
import threads.lite.dht.Util;


@RunWith(AndroidJUnit4.class)
public class IpfsUtilsTest {
    private static final String TAG = IpfsUtilsTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void decode_name() throws IOException {

        IPFS ipfs = TestEnv.getTestInstance(context);

        // test of https://docs.ipfs.io/how-to/address-ipfs-on-web/#http-gateways
        PeerId test = PeerId.fromBase58("QmNnooDu7bfjPFoTZYxMNLWUQJyrVwtbZg5gBMjTezGAJN");
        assertEquals("k2k4r8jl0yz8qjgqbmc2cdu5hkqek5rj6flgnlkyywynci20j0iuyfuj", test.toBase36());

        Cid cid = Cid.decode("QmbWqxBEKC3P8tqsKc98xmWNzrzDtRLMiMPL8wBuTGsMnR");

        assertEquals(cid.String(),
                "QmbWqxBEKC3P8tqsKc98xmWNzrzDtRLMiMPL8wBuTGsMnR");
        assertEquals(Cid.newCidV1(cid.getType(), cid.bytes()).String(),
                "bafybeigdyrzt5sfp7udm7hu76uh7y26nf3efuylqabf3oclgtqy55fbzdi");

        String self = ipfs.self().toBase58();
        assertEquals(self, ipfs.decodeName(ipfs.self().toBase36()));
    }

    @Test
    public void network() throws UnknownHostException {

        int port = 4001;

        IPFS ipfs = TestEnv.getTestInstance(context);
        PeerId peerId = ipfs.self();

        Multiaddr ma = Multiaddr.getSiteLocalAddress(peerId, port);
        assertNotNull(ma);
        LogUtils.error(TAG, ma.toString());

        ma = Multiaddr.getLocalHost(port);
        assertNotNull(ma);
        LogUtils.error(TAG, ma.toString());


    }

    @Test
    public void cat_utils() throws IOException, NoSuchAlgorithmException {

        IPFS ipfs = TestEnv.getTestInstance(context);

        PeerId peerId = ipfs.self();

        Peer peer = Peer.create(peerId, Collections.emptyList());

        ID a = ID.convertPeerID(peerId);
        ID b = ID.convertPeerID(peerId);


        BigInteger dist = Util.distance(a, b);
        assertEquals(dist.longValue(), 0L);

        int res = Util.commonPrefixLen(a, b);
        assertEquals(res, (a.data.length * 8));

        int cmp = a.compareTo(b);
        assertEquals(0, cmp);


        PeerId randrom = PeerId.random();
        Peer randomPeer = Peer.create(peerId, Collections.emptyList());
        ID r1 = ID.convertPeerID(randrom);
        ID r2 = ID.convertPeerID(randrom);

        BigInteger distCmp = Util.distance(a, r1);
        assertNotEquals(distCmp.longValue(), 0L);

        int rres = Util.commonPrefixLen(r1, r2);
        assertEquals(rres, (r1.data.length * 8));

        int rcmp = r1.compareTo(r2);
        assertEquals(0, rcmp);

        PeerDistanceSorter pds = new PeerDistanceSorter(a);
        pds.appendPeer(peer);
        pds.appendPeer(randomPeer);
        Collections.sort(pds);
        assertEquals(pds.get(0).getPeer(), peer);
        assertEquals(pds.get(1).getPeer(), randomPeer);


        PeerDistanceSorter pds2 = new PeerDistanceSorter(a);
        pds2.appendPeer(randomPeer);
        pds2.appendPeer(peer);
        Collections.sort(pds2);
        assertEquals(pds2.get(0).getPeer(), peer);
        assertEquals(pds2.get(1).getPeer(), randomPeer);


        PeerDistanceSorter pds3 = new PeerDistanceSorter(r1);
        pds3.appendPeer(peer);
        pds3.appendPeer(randomPeer);
        Collections.sort(pds3);
        assertEquals(pds3.get(0).getPeer(), randomPeer);
        assertEquals(pds3.get(1).getPeer(), peer);


        Cid cid = Cid.nsToCid("time");
        assertNotNull(cid);
        assertEquals(cid.getPrefix().version, 1);

    }


    public static class PeerDistanceSorter extends ArrayList<QueryPeer> {
        private final ID key;

        public PeerDistanceSorter(@NonNull ID key) {
            this.key = key;
        }

        @NonNull
        @Override
        public String toString() {
            return "PeerDistanceSorter{" +
                    "target=" + key +
                    '}';
        }

        public void appendPeer(@NonNull Peer peer) {
            this.add(QueryPeer.create(peer, key));
        }
    }

}
