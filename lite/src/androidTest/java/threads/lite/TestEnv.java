package threads.lite;

import android.content.Context;

import androidx.annotation.NonNull;

import java.util.concurrent.atomic.AtomicBoolean;

class TestEnv {

    private static final AtomicBoolean bootstrap = new AtomicBoolean(false);
    private static final String TAG = TestEnv.class.getSimpleName();


    public static IPFS getTestInstance(@NonNull Context context) {

        IPFS ipfs = IPFS.getInstance(context);

        ipfs.clearDatabase();

        if (!bootstrap.getAndSet(true)) {
            ipfs.updateNetwork();
            long value = ipfs.bootstrap(ipfs.getSession());

            LogUtils.debug(TAG, "Bootstrap time in minutes : " + value);
            LogUtils.debug(TAG, "Server runs on port " + ipfs.getPort());

            ipfs.setListenAddresses(inetAddresses -> LogUtils.error(TAG, inetAddresses.toString()));
        }

        return ipfs;
    }


}
