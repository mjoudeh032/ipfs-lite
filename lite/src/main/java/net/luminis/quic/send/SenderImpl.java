/*
 * Copyright © 2020, 2021, 2022 Peter Doornbosch
 *
 * This file is part of Kwik, an implementation of the QUIC protocol in Java.
 *
 * Kwik is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Kwik is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package net.luminis.quic.send;

import static java.lang.Long.max;

import net.luminis.quic.EncryptionLevel;
import net.luminis.quic.GlobalAckGenerator;
import net.luminis.quic.IdleTimer;
import net.luminis.quic.PnSpace;
import net.luminis.quic.QuicConnectionImpl;
import net.luminis.quic.Version;
import net.luminis.quic.cc.CongestionControlEventListener;
import net.luminis.quic.cc.CongestionController;
import net.luminis.quic.cc.NewRenoCongestionController;
import net.luminis.quic.crypto.ConnectionSecrets;
import net.luminis.quic.crypto.Keys;
import net.luminis.quic.frame.QuicFrame;
import net.luminis.quic.packet.RetryPacket;
import net.luminis.quic.recovery.RecoveryManager;
import net.luminis.quic.recovery.RttEstimator;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Consumer;
import java.util.function.Function;

import threads.lite.LogUtils;

/**
 * Sender implementation that queues frames-to-be-sent and assembles packets "just in time" when conditions allow to
 * send a packet.
 * <p>
 * Sending packets is limited by congestion controller, anti-amplification attack limitations and, for stream frames,
 * flow control. However, ack-only packets are not subject to congestion control and probes are not limited by
 * congestion control (but do count); therefore, such packets have priority when other packets are queued because of
 * congestion control limits. Additionally, delayed ack packets don't have to be send immediately, but they have to
 * within a given time frame.
 * To improve packet and frame coalescing, messages should not be sent immediately when there is the expectation that
 * more will follow in due time.
 * <p>
 * So a sender has to wait for any of the following conditions to become true:
 * - received packet completely processed or batch of packets processed and send requests queued
 * - "spontaneous" request queued (e.g. application initiated stream data)
 * - probe request
 * - delayed ack timeout
 * - congestion controller becoming unblocked due to timer-induced loss detection
 */
public class SenderImpl implements Sender, CongestionControlEventListener {
    private static final AtomicInteger INSTANCES = new AtomicInteger(0);
    private static final String TAG = SenderImpl.class.getSimpleName();
    private final int maxPacketSize;
    private final InetSocketAddress peerAddress;
    private final CongestionController congestionController;
    private final RttEstimator rttEstimater;
    private final SendRequestQueue[] sendRequestQueue = new SendRequestQueue[EncryptionLevel.values().length];
    private final GlobalPacketAssembler packetAssembler;
    private final GlobalAckGenerator globalAckGenerator;
    private final RecoveryManager recoveryManager;
    private final IdleTimer idleTimer;
    private final Thread senderThread;
    private final boolean[] discardedSpaces = new boolean[PnSpace.pnSpaces().size()];
    private final ReentrantReadWriteLock readWriteDiscardLock = new ReentrantReadWriteLock();
    private final Object condition = new Object();
    private final AtomicInteger subsequentZeroDelays = new AtomicInteger();
    private final AtomicLong bytesSent = new AtomicLong(0);
    private final DatagramSocket socket;
    // Using thread-confinement strategy for concurrency control: only the sender thread created in this class accesses these members
    private final AtomicBoolean running = new AtomicBoolean(false);
    private final AtomicBoolean lastDelayWasZero = new AtomicBoolean(false);
    private final Consumer<Throwable> abortCallback;
    private final QuicConnectionImpl connection;
    private final AtomicInteger receiverMaxAckDelay = new AtomicInteger();
    private final AtomicInteger antiAmplificationLimit = new AtomicInteger(-1);
    private final AtomicBoolean signalled = new AtomicBoolean();
    private final ConnectionSecrets connectionSecrets;

    public SenderImpl(Version version, ConnectionSecrets connectionSecrets, IdleTimer idleTimer,
                      QuicConnectionImpl connection, int maxPacketSize,
                      DatagramSocket socket, InetSocketAddress peerAddress,
                      Integer initialRtt, Consumer<Throwable> abortCallback) {
        this.connectionSecrets = connectionSecrets;
        this.idleTimer = idleTimer;
        this.connection = connection;
        this.maxPacketSize = maxPacketSize;
        this.socket = socket;
        this.peerAddress = peerAddress;
        this.abortCallback = abortCallback;


        idleTimer.setPtoSupplier(this::getPto);
        EncryptionLevel.encryptionLevels().forEach(level -> {
            int levelIndex = level.ordinal();
            sendRequestQueue[levelIndex] = new SendRequestQueue(level);
        });
        globalAckGenerator = new GlobalAckGenerator(this);
        packetAssembler = new GlobalPacketAssembler(version, sendRequestQueue, globalAckGenerator);

        congestionController = new NewRenoCongestionController(this);
        rttEstimater = (initialRtt == null) ? new RttEstimator() : new RttEstimator(initialRtt);

        recoveryManager = new RecoveryManager(connection.getRole(), rttEstimater,
                congestionController, this);
        connection.registerProcessor(recoveryManager);
        connection.addHandshakeStateListener(recoveryManager);

        senderThread = new Thread(this::run, "sender-loop");
        senderThread.setDaemon(true);
    }

    public void start() {
        this.running.set(true);
        this.senderThread.start();
    }

    @Override
    public void send(QuicFrame frame, EncryptionLevel level) {
        sendRequestQueue[level.ordinal()].addRequest(frame, f -> {
        });
    }

    @Override
    public void send(QuicFrame frame, EncryptionLevel level, Consumer<QuicFrame> frameLostCallback) {
        sendRequestQueue[level.ordinal()].addRequest(frame, frameLostCallback);
    }

    @Override
    public void send(Function<Integer, QuicFrame> frameSupplier, int minimumSize,
                     EncryptionLevel level, Consumer<QuicFrame> lostCallback) {
        sendRequestQueue[level.ordinal()].addRequest(frameSupplier, minimumSize, lostCallback);
    }

    public void send(RetryPacket retryPacket) {
        try {
            send(List.of(new SendItem(retryPacket)));
        } catch (IOException e) {
            LogUtils.error(TAG, "Sending packet failed: " + retryPacket);
        }
    }

    @Override
    public void setInitialToken(byte[] token) {
        if (token != null) {
            packetAssembler.setInitialToken(token);
        }
    }

    @Override
    public void sendAck(PnSpace pnSpace, int maxDelay) {
        sendRequestQueue[Objects.requireNonNull(pnSpace.relatedEncryptionLevel()).ordinal()].addAckRequest(maxDelay);
        // Now, the sender loop must use a different wait-period, to ensure it wakes up when the delayed ack
        // must be sent.
        // However, given the current implementation of packetProcessed (i.e. it always wakes up the sender loop),
        // it is not necessary to do this with a ...
        // senderThread.interrupt
        // ... because packetProcessed will ensure the new period is computed.
    }

    @Override
    public void sendProbe(List<QuicFrame> frames, EncryptionLevel level) {
        readWriteDiscardLock.readLock().lock();
        try {
            if (!discardedSpaces[level.relatedPnSpace().ordinal()]) {
                sendRequestQueue[level.ordinal()].addProbeRequest(frames);
                wakeUpSenderLoop();
            } else {
                LogUtils.warning(TAG, "Attempt to send probe on discarded space (" +
                        level.relatedPnSpace() + ") => ignoring");
            }
        } finally {
            readWriteDiscardLock.readLock().unlock();
        }

    }

    @Override
    public void packetProcessed(boolean expectingMore) {
        wakeUpSenderLoop();  // If you change this, review this.sendAck()!
    }

    @Override
    public void flush() {
        wakeUpSenderLoop();
    }

    public void discard(PnSpace space, String reason) {
        readWriteDiscardLock.writeLock().lock();
        try {
            if (!discardedSpaces[space.ordinal()]) {
                packetAssembler.stop(space);
                recoveryManager.stopRecovery(space);
                LogUtils.debug(TAG, "Discarding pn space " + space + " because " + reason);
                globalAckGenerator.discard(space);
                discardedSpaces[space.ordinal()] = true;
            }
        } finally {
            readWriteDiscardLock.writeLock().unlock();
        }
    }

    /**
     * Stop sending packets, but don't shutdown yet, so connection close can be sent.
     */
    public void stop() {
        // Stop sending packets, so discard any packet waiting to be send.
        Arrays.stream(sendRequestQueue).forEach(SendRequestQueue::clear);

        // No more retransmissions either.
        recoveryManager.stopRecovery();
    }

    public void shutdown() {
        // Stopped should have be called before.
        // Stop cannot be called here (again), because it would drop ConnectionCloseFrame still waiting to be sent.

        running.set(false);
        senderThread.interrupt();
        // todo ReWi check
        globalAckGenerator.discard();
    }

    @Override
    public void bytesInFlightIncreased(long bytesInFlight) {
    }

    @Override
    public void bytesInFlightDecreased(long bytesInFlight) {
        wakeUpSenderLoop();
    }

    private void run() {
        LogUtils.debug(TAG, "Instances " + INSTANCES.incrementAndGet());
        try {
            // Determine whether this loop must be ended _before_ composing packets, to avoid race conditions with
            // items being queued just after the packet assembler (for that level) has executed.
            while (running.get()) {

                synchronized (condition) {
                    try {
                        // TODO looks shitty here
                        if (!signalled.get()) {
                            long timeout = determineMinimalDelay();
                            if (timeout > 0) {
                                condition.wait(timeout);
                            }
                        }
                        signalled.set(false);
                        // TODO looks shitty here
                    } catch (InterruptedException e) {
                        LogUtils.debug(TAG, "Sender thread is interrupted; probably shutting down? " + running);
                    }
                }
                sendIfAny();
            }
        } catch (Throwable fatalError) {
            if (running.get()) {
                LogUtils.debug(TAG, "Sender thread aborted with exception " +
                        peerAddress.toString());
                abortCallback.accept(fatalError);
            } else {
                LogUtils.warning(TAG, "Ignoring " + fatalError +
                        " because sender is shutting down.");
            }
        } finally {
            LogUtils.debug(TAG, "Instances " + INSTANCES.decrementAndGet());
        }
    }

    void sendIfAny() throws IOException {
        List<SendItem> items;
        do {
            items = assemblePacket();
            if (!items.isEmpty()) {
                send(items);
            }
        }
        while (!items.isEmpty());
    }

    private void wakeUpSenderLoop() {
        synchronized (condition) {
            signalled.set(true);
            condition.notify();
        }
    }

    private long determineMinimalDelay() {
        Optional<Instant> nextDelayedSendTime = packetAssembler.nextDelayedSendTime();
        if (nextDelayedSendTime.isPresent()) {
            long delay = max(Duration.between(Instant.now(),
                    nextDelayedSendTime.get()).toMillis(), 0);
            if (delay > 0) {
                subsequentZeroDelays.set(0);
                lastDelayWasZero.set(false);
                return delay;
            } else {
                if (lastDelayWasZero.get()) {
                    int count = subsequentZeroDelays.incrementAndGet();
                    if (count % 20 == 3) {
                        LogUtils.error(TAG, "possible bug: sender is " +
                                "looping in busy wait; got " + count + " iterations");
                    }
                    if (count > 10003) {
                        return 8000;
                    }
                }
                lastDelayWasZero.set(true);
                // Next time is already in the past, hurry up!
                return 0;
            }
        }

        // No timeout needed, just wait for next action. In theory, infinity should be returned.
        // However, in order to somewhat forgiving for bugs that would lead to deadlocking the sender, use a
        // value that will keep the connection going, but also indicates there is something wrong.
        return 5000;
    }

    void send(List<SendItem> itemsToSend) throws IOException {
        byte[] datagramData = new byte[maxPacketSize];
        ByteBuffer buffer = ByteBuffer.wrap(datagramData);
        try {
            itemsToSend.stream()
                    .map(SendItem::getPacket)
                    .forEach(packet -> {
                        Keys keys = connectionSecrets.getOwnSecrets(packet.getEncryptionLevel());
                        if (keys == null) {
                            throw new IllegalStateException("Missing keys for encryption level "
                                    + packet.getEncryptionLevel());
                        }
                        byte[] packetData = packet.generatePacketBytes(packet.getPacketNumber(), keys);
                        buffer.put(packetData);
                    });
        } catch (BufferOverflowException bufferOverflow) {
            LogUtils.error(TAG, "Buffer overflow while generating datagram for " + itemsToSend);
            throw bufferOverflow;
        }
        DatagramPacket datagram = new DatagramPacket(datagramData, buffer.position(),
                peerAddress.getAddress(), peerAddress.getPort());

        Instant timeSent = Instant.now();
        socket.send(datagram);
        bytesSent.addAndGet(buffer.position());

        itemsToSend.forEach(item -> {
            recoveryManager.packetSent(item.getPacket(), timeSent, item.getPacketLostCallback());
            idleTimer.packetSent(item.getPacket(), timeSent);
        });
    }

    private List<SendItem> assemblePacket() {
        int remainingCwnd = (int) congestionController.remainingCwnd();
        int currentMaxPacketSize = maxPacketSize;
        int antiAmplificationLimitCwnd = antiAmplificationLimit.get();
        if (antiAmplificationLimitCwnd >= 0) {
            long send = bytesSent.get();
            if (send < antiAmplificationLimitCwnd) {
                currentMaxPacketSize = Integer.min(currentMaxPacketSize, (int)
                        (antiAmplificationLimitCwnd - send));
            } else {
                LogUtils.error(TAG, "Cannot send; anti-amplification limit is reached");
                return Collections.emptyList();
            }
        }
        byte[] srcCid = connection.getSourceConnectionId();
        byte[] destCid = connection.getDestinationConnectionId();
        return packetAssembler.assemble(remainingCwnd, currentMaxPacketSize, srcCid, destCid);
    }


    public int getPto() {
        return rttEstimater.getSmoothedRtt() + 4 * rttEstimater.getRttVar() + receiverMaxAckDelay.get();
    }

    public CongestionController getCongestionController() {
        return congestionController;
    }

    public void setReceiverMaxAckDelay(int maxAckDelay) {
        receiverMaxAckDelay.set(maxAckDelay);
        rttEstimater.setMaxAckDelay(maxAckDelay);
    }

    public GlobalAckGenerator getGlobalAckGenerator() {
        return globalAckGenerator;
    }

    public void setAntiAmplificationLimit(int antiAmplificationLimit) {
        this.antiAmplificationLimit.set(antiAmplificationLimit);
    }

    public void unsetAntiAmplificationLimit() {
        antiAmplificationLimit.set(-1);
    }

    public void enableAllLevels() {
        packetAssembler.enableAppLevel();
    }

    public void enableAppLevel() {
        packetAssembler.enableAppLevel();
    }

}

