package threads.lite.ident;

import androidx.annotation.NonNull;

import com.google.protobuf.ByteString;

import net.luminis.quic.QuicConnection;
import net.luminis.quic.QuicStream;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import identify.pb.IdentifyOuterClass;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Multiaddr;
import threads.lite.host.PeerInfo;
import threads.lite.host.StreamData;
import threads.lite.host.StreamDataHandler;
import threads.lite.utils.DataHandler;

public class IdentityService {

    @NonNull
    public static CompletableFuture<PeerInfo> getPeerInfo(@NonNull QuicConnection conn) {

        return IdentityService.getIdentity(conn).thenApply(IdentityService::getPeerInfo);
    }

    @NonNull
    private static PeerInfo getPeerInfo(@NonNull IdentifyOuterClass.Identify identify) {

        String agent = identify.getAgentVersion();
        String version = identify.getProtocolVersion();
        Multiaddr observedAddr = null;
        if (identify.hasObservedAddr()) {
            try {
                observedAddr = Multiaddr.createMultiaddr(identify.getObservedAddr());
            } catch (Throwable ignore) {
            }
        }

        List<String> protocols = identify.getProtocolsList();
        List<Multiaddr> addresses = new ArrayList<>();
        for (ByteString entry : identify.getListenAddrsList()) {
            try {
                addresses.add(Multiaddr.createMultiaddr(entry));
            } catch (Throwable ignore) {
            }
        }

        return new PeerInfo(agent, version, addresses, protocols, observedAddr);
    }

    @NonNull
    public static CompletableFuture<IdentifyOuterClass.Identify> getIdentity(
            @NonNull QuicConnection conn) {

        CompletableFuture<IdentifyOuterClass.Identify> done = new CompletableFuture<>();
        conn.createStream(new StreamDataHandler(new StreamData() {
                    @Override
                    public void throwable(Throwable throwable) {
                        LogUtils.error(IdentityService.class.getSimpleName(), throwable);
                        done.completeExceptionally(throwable);
                    }

                    @Override
                    public void token(QuicStream stream, String token) throws Exception {
                        if (!Arrays.asList(IPFS.STREAM_PROTOCOL, IPFS.IDENTITY_PROTOCOL).contains(token)) {
                            throw new Exception("Token " + token + " not supported");
                        }
                        if (Objects.equals(token, IPFS.IDENTITY_PROTOCOL)) {
                            stream.closeOutput();
                        }
                    }

                    @Override
                    public void fin() {
                        // nothing to do here
                    }

                    @Override
                    public void data(QuicStream stream, ByteBuffer data) throws Exception {
                        done.complete(IdentifyOuterClass.Identify.parseFrom(data.array()));
                    }

                }), true, IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS)
                .thenApply(quicStream -> quicStream.writeOutput(
                        DataHandler.writeToken(IPFS.STREAM_PROTOCOL, IPFS.IDENTITY_PROTOCOL)));

        return done;

    }
}
