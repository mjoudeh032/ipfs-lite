package threads.lite.utils;

import android.util.Pair;

import androidx.annotation.NonNull;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import merkledag.pb.Merkledag;
import threads.lite.cid.Cid;
import threads.lite.core.Closeable;
import threads.lite.dag.BlockService;
import threads.lite.dag.DagReader;
import threads.lite.dag.NodeService;
import threads.lite.format.BlockStore;
import threads.lite.host.Session;


public class Resolver {

    @NonNull
    public static Cid resolveNode(@NonNull Closeable closeable, @NonNull BlockStore blockStore,
                                  @NonNull Session session, @NonNull Cid root,
                                  @NonNull List<String> path) throws InterruptedException {

        BlockService blockservice = BlockService.createBlockService(blockStore, session);
        NodeService nodeService = NodeService.createNodeService(blockservice);
        Pair<Cid, List<String>> resolved = resolveToLastNode(closeable, nodeService, root, path);
        Cid cid = resolved.first;
        Objects.requireNonNull(cid);
        // make sure it is resolved
        Merkledag.PBNode res = resolveNode(closeable, nodeService, cid);
        Objects.requireNonNull(res);
        return cid;
    }

    @NonNull
    public static Merkledag.PBNode resolveNode(@NonNull Closeable closeable,
                                               @NonNull NodeService nodeGetter,
                                               @NonNull Cid cid) throws InterruptedException {
        return nodeGetter.getNode(closeable, cid);
    }

    @NonNull
    private static Pair<Cid, List<String>> resolveToLastNode(@NonNull Closeable closeable,
                                                             @NonNull NodeService nodeService,
                                                             @NonNull Cid root,
                                                             @NonNull List<String> path)
            throws InterruptedException {

        if (path.size() == 0) {
            return Pair.create(root, Collections.emptyList());
        }

        Cid cid = root;
        Merkledag.PBNode node = nodeService.getNode(closeable, cid);

        for (String name : path) {
            Merkledag.PBLink lnk = DagReader.getLinkByName(node, name);
            Objects.requireNonNull(lnk);
            cid = new Cid(lnk.getHash().toByteArray());
            node = nodeService.getNode(closeable, cid);
        }

        return Pair.create(cid, Collections.emptyList());

    }

}
