package threads.lite.bitswap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import net.luminis.quic.QuicConnection;
import net.luminis.quic.QuicStream;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;

import bitswap.pb.MessageOuterClass;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.core.Closeable;
import threads.lite.data.BlockSupplier;
import threads.lite.format.Block;
import threads.lite.format.BlockStore;
import threads.lite.host.LiteHost;
import threads.lite.host.Session;
import threads.lite.host.StreamDataHandler;
import threads.lite.host.TokenData;
import threads.lite.utils.DataHandler;


public class BitSwapManager implements BitSwap {

    private static final String TAG = BitSwapManager.class.getSimpleName();
    @NonNull
    private final LiteHost host;
    @NonNull
    private final Session session;
    @NonNull
    private final BlockStore blockStore;
    @NonNull
    private final Set<Multiaddr> motherfuckers = ConcurrentHashMap.newKeySet();
    @NonNull
    private final ConcurrentHashMap<Multiaddr, QuicConnection> peers = new ConcurrentHashMap<>();
    @NonNull
    private final BitSwapEngine engine;
    private final boolean findProvidersActive;
    @NonNull
    private final Registry registry = new Registry();
    @NonNull
    private final Consumer<BlockSupplier> supplier;

    public BitSwapManager(@NonNull LiteHost host,
                          @NonNull Session session,
                          @NonNull BlockStore blockStore,
                          @NonNull Consumer<BlockSupplier> supplier,
                          boolean findProvidersActive) {
        this.host = host;
        this.session = session;
        this.blockStore = blockStore;
        this.engine = new BitSwapEngine(blockStore);
        this.supplier = supplier;
        this.findProvidersActive = findProvidersActive;

    }

    private static void writeMessage(@NonNull QuicConnection conn, @NonNull BitSwapMessage message) {

        if (IPFS.BITSWAP_SEND_REQUEST_ACTIVE) {
            try {
                conn.createStream(new StreamDataHandler(new TokenData() {
                            @Override
                            public void throwable(Throwable throwable) {
                                LogUtils.error(TAG, throwable);
                                conn.close();
                            }

                            @Override
                            public void token(QuicStream stream, String token) throws Exception {
                                if (!Arrays.asList(IPFS.STREAM_PROTOCOL,
                                        IPFS.BITSWAP_PROTOCOL).contains(token)) {
                                    throw new Exception("Token " + token + " not supported");
                                }
                                if (Objects.equals(token, IPFS.BITSWAP_PROTOCOL)) {
                                    stream.writeOutput(DataHandler.encode(message.toProtoV1()))
                                            .thenApply(QuicStream::closeOutput);
                                }
                            }

                            @Override
                            public void fin() {
                                // nothing yet to do
                            }
                        }), true, IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS)
                        .thenApply(quicStream -> quicStream.writeOutput(
                                DataHandler.writeToken(IPFS.STREAM_PROTOCOL, IPFS.BITSWAP_PROTOCOL)));

            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable.getClass().getSimpleName() +
                        " : " + throwable.getMessage());
            }
        }
    }

    public static void sendWants(@NonNull QuicConnection conn, @NonNull List<Cid> wants) {

        if (wants.size() == 0) {
            return;
        }
        BitSwapMessage message = BitSwapMessage.create();

        int priority = Integer.MAX_VALUE;

        for (Cid c : wants) {

            message.entry(c, priority, MessageOuterClass.Message.Wantlist.WantType.Block,
                    false);

            priority--;
        }

        if (message.empty()) {
            return;
        }

        writeMessage(conn, message);

    }

    public void clear() {
        try {
            peers.values().forEach(QuicConnection::close);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            registry.clear();
            peers.clear();
            motherfuckers.clear();
        }
    }

    private void findProviders(@NonNull ScheduledExecutorService executorService,
                               @NonNull AtomicBoolean findProviders,
                               @NonNull Closeable closeable,
                               @NonNull Cid cid,
                               int delay) {
        executorService.schedule(() -> {

            long start = System.currentTimeMillis();
            try {
                LogUtils.debug(TAG, "Load Provider Start " + cid.String());

                if (closeable.isClosed()) {
                    return;
                }

                session.findProviders(closeable, (multiaddr) -> {

                    if (motherfuckers.contains(multiaddr)) {
                        // not possible to connect
                        return;
                    }

                    QuicConnection conn = peers.get(multiaddr);
                    if (conn != null) {
                        if (!conn.isConnected()) {
                            peers.remove(multiaddr);
                        } else {
                            // nothing to do here there
                            return;
                        }
                    }

                    if (closeable.isClosed()) {
                        return;
                    }

                    host.connect(session, Set.of(multiaddr), IPFS.CONNECT_TIMEOUT,
                            IPFS.GRACE_PERIOD, IPFS.MAX_STREAMS,
                            IPFS.MESSAGE_SIZE_MAX).whenComplete((connection, throwable) -> {
                        if (throwable != null) {
                            motherfuckers.add(multiaddr);
                        } else {
                            LogUtils.error(TAG, "New connection " + multiaddr);
                            peers.put(multiaddr, connection);
                        }
                    });

                }, cid);

                if (closeable.isClosed()) {
                    return;
                }

                if (!executorService.isShutdown()) {
                    findProviders.set(false);
                }

            } catch (InterruptedException ignore) {
                // nothing to do here
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            } finally {
                LogUtils.info(TAG, "Load Provider Finish " + cid.String() +
                        " onStart [" + (System.currentTimeMillis() - start) + "]...");
            }
        }, delay, TimeUnit.MILLISECONDS);
    }

    public Block runWantHaves(@NonNull Closeable closeable, @NonNull Cid cid) throws InterruptedException {

        registry.register(cid);
        ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

        AtomicBoolean findProviders = new AtomicBoolean(false);

        try {

            Set<QuicConnection> haves = new HashSet<>();

            while (!blockStore.hasBlock(cid)) {

                if (closeable.isClosed()) {
                    throw new InterruptedException();
                }

                // fill the peers with the swarm connections
                session.getSwarm().forEach(connection -> peers.putIfAbsent(
                        Multiaddr.create(connection.getRemoteAddress()), connection));

                for (Multiaddr multiaddr : peers.keySet()) {
                    QuicConnection conn = peers.get(multiaddr);
                    if (conn != null) {
                        if (!conn.isConnected()) {
                            peers.remove(multiaddr);
                            continue;
                        }
                        if (!haves.contains(conn)) {
                            haves.add(conn);
                            sendHaves(conn, Collections.singletonList(cid));
                        }
                    }
                }

                if (findProvidersActive) {
                    try {
                        int delay = 0;
                        if (!peers.isEmpty()) {
                            delay = IPFS.BITSWAP_SEARCH_DELAY;
                        }
                        if (!findProviders.getAndSet(true)) {
                            findProviders(executorService, findProviders, closeable, cid, delay);
                        }
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                }
            }
        } finally {
            executorService.shutdown();
            executorService.shutdownNow();
            registry.unregister(cid);
        }

        return blockStore.getBlock(cid);
    }


    @Nullable
    public Block getBlock(@NonNull Closeable closeable, @NonNull Cid cid) throws InterruptedException {

        try {
            Block block = blockStore.getBlock(cid);
            if (block == null) {
                AtomicBoolean done = new AtomicBoolean(false);
                LogUtils.info(TAG, "Block Get " + cid.String());

                try {
                    return runWantHaves(() -> closeable.isClosed() || done.get(), cid);
                } finally {
                    done.set(true);
                }
            }
            return block;

        } finally {
            LogUtils.info(TAG, "Block Release  " + cid.String());
        }
    }

    public void receiveMessage(@NonNull QuicConnection conn, @NonNull BitSwapMessage bsm) {

        for (Block block : bsm.blocks()) {
            Cid cid = block.getCid();
            if (registry.isRegistered(cid)) {
                blockStore.putBlock(block);
                registry.unregister(cid);
                supplier.accept(new BlockSupplier(cid, conn.getRemoteAddress()));
            }
        }

        for (Cid cid : bsm.haves()) {
            if (registry.isRegistered(cid)) {
                registry.scheduleWants(cid, conn);
            }
        }

        engine.receiveMessage(conn, bsm);
    }


    private void sendHaves(@NonNull QuicConnection conn, @NonNull List<Cid> haves) {
        if (haves.size() == 0) {
            return;
        }

        int priority = Integer.MAX_VALUE;

        BitSwapMessage message = BitSwapMessage.create();

        for (Cid c : haves) {

            // Broadcast wants are sent as want-have
            MessageOuterClass.Message.Wantlist.WantType wantType =
                    MessageOuterClass.Message.Wantlist.WantType.Have;

            message.entry(c, priority, wantType, false);

            priority--;
        }

        if (message.empty()) {
            return;
        }

        writeMessage(conn, message);

    }

    private static class Registry extends ConcurrentHashMap<Cid, Timer> {
        private final ConcurrentHashMap<Cid, Integer> delays = new ConcurrentHashMap<>();

        public void register(@NonNull Cid cid) {
            this.put(cid, new Timer());
            this.delays.put(cid, 0);
        }

        public void unregister(@NonNull Cid cid) {
            Timer timer = this.remove(cid);
            if (timer != null) {
                timer.cancel();
            }
            this.delays.remove(cid);
        }

        public boolean isRegistered(@NonNull Cid cid) {
            return this.containsKey(cid);
        }

        public void clear() {
            values().forEach(Timer::cancel);
            super.clear();
            delays.clear();
        }

        public void scheduleWants(@NonNull Cid cid, @NonNull QuicConnection conn) {
            Timer timer = this.get(cid);
            if (timer != null) {
                int delay = delays.computeIfAbsent(cid, cid1 -> 0);
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        sendWants(conn, Collections.singletonList(cid));
                    }
                }, delay);
                delays.put(cid, delay + IPFS.BITSWAP_WANTS_DELAY);
            }
        }
    }
}
