package threads.lite.bitswap;

import androidx.annotation.Nullable;

import net.luminis.quic.QuicConnection;

import threads.lite.cid.Cid;
import threads.lite.core.Closeable;
import threads.lite.format.Block;

public interface BitSwap {

    @Nullable
    Block getBlock(Closeable closeable, Cid cid) throws InterruptedException;

    void clear();

    void receiveMessage(QuicConnection conn, BitSwapMessage bsm);
}
