package threads.lite.data;

import androidx.annotation.NonNull;

import java.net.InetSocketAddress;

import threads.lite.cid.Cid;

public class BlockSupplier {
    private final Cid cid;
    private final InetSocketAddress address;

    public BlockSupplier(Cid cid, InetSocketAddress address) {
        this.cid = cid;
        this.address = address;
    }

    public Cid getCid() {
        return cid;
    }

    @NonNull
    @Override
    public String toString() {
        return "DataSupplier{" +
                "cid=" + cid.String() +
                ", address=" + address +
                '}';
    }

    public InetSocketAddress getAddress() {
        return address;
    }
}
