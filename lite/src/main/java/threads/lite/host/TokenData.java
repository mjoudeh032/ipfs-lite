package threads.lite.host;

import net.luminis.quic.QuicStream;

public interface TokenData {
    void throwable(Throwable throwable);

    void token(QuicStream stream, String token) throws Exception;

    void fin();

}
