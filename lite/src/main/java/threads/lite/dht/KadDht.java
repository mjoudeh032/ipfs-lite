package threads.lite.dht;

import android.annotation.SuppressLint;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.protobuf.ByteString;

import net.luminis.quic.QuicConnection;
import net.luminis.quic.QuicStream;

import java.net.ConnectException;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;

import dht.pb.Dht;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Cid;
import threads.lite.cid.ID;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.Peer;
import threads.lite.cid.PeerId;
import threads.lite.cid.Protocol;
import threads.lite.core.Closeable;
import threads.lite.host.DnsResolver;
import threads.lite.host.LiteHost;
import threads.lite.host.StreamData;
import threads.lite.host.StreamDataHandler;
import threads.lite.host.TokenData;
import threads.lite.ipns.Ipns;
import threads.lite.ipns.Validator;
import threads.lite.utils.DataHandler;


public class KadDht {

    private static final String TAG = KadDht.class.getSimpleName();
    public final LiteHost host;
    public final PeerId self;
    @NonNull
    public final RoutingTable routingTable = new RoutingTable();
    @NonNull
    private final Set<PeerId> motherfuckers = ConcurrentHashMap.newKeySet();
    private final Validator validator;

    @NonNull
    private final ReentrantLock lock = new ReentrantLock();

    public KadDht(@NonNull LiteHost host, @NonNull Validator validator) {
        this.host = host;
        this.validator = validator;
        this.self = host.self();
    }

    public void clear() {
        motherfuckers.clear();
        routingTable.clear();
    }

    void bootstrap() {
        // Fill routing table with currently connected peers that are DHT servers
        if (routingTable.isEmpty()) {
            try {
                lock.lock();

                try {
                    Set<String> addresses = new HashSet<>(IPFS.DHT_BOOTSTRAP_NODES);

                    for (String multiAddress : addresses) {
                        try {
                            Multiaddr multiaddr = new Multiaddr(multiAddress);
                            String name = multiaddr.getStringComponent(Protocol.P2P);
                            Objects.requireNonNull(name);
                            PeerId peerId = PeerId.fromBase58(name);
                            Objects.requireNonNull(peerId);

                            List<Multiaddr> multiaddrs =
                                    DnsResolver.resolveDnsAddress(multiaddr);
                            if (!multiaddrs.isEmpty()) {
                                Peer peer = Peer.create(peerId, multiaddrs);
                                peer.setReplaceable(false);
                                routingTable.addPeer(peer);
                            }
                        } catch (Throwable throwable) {
                            LogUtils.error(TAG, throwable);
                        }
                    }
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }

            } finally {
                lock.unlock();
            }
        }
    }


    @NonNull
    private List<Peer> evalClosestPeers(@NonNull Dht.Message pms) {

        List<Peer> peers = new ArrayList<>();
        List<Dht.Message.Peer> list = pms.getCloserPeersList();
        for (Dht.Message.Peer entry : list) {
            List<Multiaddr> multiAddresses = new ArrayList<>();
            List<ByteString> addresses = entry.getAddrsList();
            for (ByteString address : addresses) {
                Multiaddr multiaddr = createMultiaddr(address);
                if (multiaddr != null) {
                    if (multiaddr.isSupported(host.protocol.get())) {
                        multiAddresses.add(multiaddr);
                    }
                }
            }

            if (!multiAddresses.isEmpty()) {
                PeerId peerId = PeerId.create(entry.getId().toByteArray());
                if (!motherfuckers.contains(peerId)) {
                    peers.add(Peer.create(peerId, multiAddresses));
                }
            } else {
                LogUtils.info(TAG, "Ignore evalClosestPeers : " + multiAddresses);
            }
        }
        return peers;
    }


    private void getClosestPeers(@NonNull Closeable closeable, @NonNull byte[] key,
                                 @NonNull Consumer<List<Peer>> channel) throws InterruptedException {
        if (key.length == 0) {
            throw new RuntimeException("can't lookup empty key");
        }

        runQuery(closeable, key, (ctx1, p) -> {

            Dht.Message pms = findPeerSingle(ctx1, p, key);

            List<Peer> peers = evalClosestPeers(pms);

            channel.accept(peers);

            return peers;
        });


    }

    public void putValue(@NonNull Closeable closable, @NonNull byte[] key, @NonNull byte[] value)
            throws InterruptedException {

        bootstrap();

        // don't allow local users to put bad values.
        try {
            Ipns.Entry entry = validator.validate(key, value);
            Objects.requireNonNull(entry);
        } catch (Throwable throwable) {
            throw new RuntimeException(throwable);
        }

        long start = System.currentTimeMillis();

        @SuppressLint("SimpleDateFormat") String format = new SimpleDateFormat(
                IPFS.TIME_FORMAT_IPFS).format(new Date());
        Dht.Message.Record rec = Dht.Message.Record.newBuilder().setKey(ByteString.copyFrom(key))
                .setValue(ByteString.copyFrom(value))
                .setTimeReceived(format).build();

        Set<PeerId> handled = ConcurrentHashMap.newKeySet();

        try {
            getClosestPeers(closable, key, peers -> {

                for (Peer peer : peers) {
                    if (peer.hasAddresses()) {
                        if (!handled.contains(peer.getPeerId())) {
                            handled.add(peer.getPeerId());
                            putValueToPeer(peer, rec);
                        }
                    }
                }
            });
        } finally {
            LogUtils.verbose(TAG, "Finish putValue at " + (System.currentTimeMillis() - start));
        }

    }

    private void putValueToPeer(@NonNull Peer peer, @NonNull Dht.Message.Record rec) {


        Dht.Message pms = Dht.Message.newBuilder()
                .setType(Dht.Message.MessageType.PUT_VALUE)
                .setKey(rec.getKey())
                .setRecord(rec)
                .setClusterLevelRaw(0).build();

        if (motherfuckers.contains(peer.getPeerId())) {
            return;
        }

        host.connect(host.createSession(), peer.getMultiaddrs(), IPFS.CONNECT_TIMEOUT, IPFS.CONNECT_TIMEOUT,
                0, 20480).whenComplete((connection, throwable) -> {
            if (throwable != null) {
                motherfuckers.add(peer.getPeerId());
            } else {
                try {
                    Dht.Message rimes = sendRequest(connection, peer, pms);

                    if (!Arrays.equals(rimes.getRecord().getValue().toByteArray(),
                            pms.getRecord().getValue().toByteArray())) {
                        LogUtils.error(TAG, "value not put correctly put-message  " +
                                pms + " get-message " + rimes);
                        peer.setLatency(Long.MAX_VALUE);
                    }
                } catch (Throwable ignore) {
                    // nothing to do here
                }
            }
        });

    }


    public void findProviders(@NonNull Closeable closeable, @NonNull Consumer<Multiaddr> consumer,
                              @NonNull Cid cid) throws InterruptedException {
        if (!cid.isDefined()) {
            throw new RuntimeException("Cid invalid");
        }

        bootstrap();


        long start = System.currentTimeMillis();
        try {
            byte[] key = cid.getHash();
            Set<Multiaddr> handled = ConcurrentHashMap.newKeySet();
            runQuery(closeable, key, (ctx, p) -> {

                Dht.Message pms = findProvidersSingle(ctx, p, key);
                List<Peer> peers = evalClosestPeers(pms);

                List<Dht.Message.Peer> list = pms.getProviderPeersList();
                for (Dht.Message.Peer entry : list) {

                    List<Multiaddr> multiAddresses = new ArrayList<>();
                    List<ByteString> addresses = entry.getAddrsList();
                    for (ByteString address : addresses) {
                        Multiaddr multiaddr = createMultiaddr(address);
                        if (multiaddr != null) {
                            if (multiaddr.isSupported(host.protocol.get())) {
                                multiAddresses.add(multiaddr);
                            }
                        }
                    }


                    for (Multiaddr ma : multiAddresses) {
                        if (!handled.contains(ma)) {
                            handled.add(ma);

                            LogUtils.error(TAG, "findProviders " +
                                    " Cid Version : " + cid.getVersion() + " peer " + ma);

                            consumer.accept(ma);
                        }
                    }
                }
                return peers;

            });
        } finally {
            LogUtils.debug(TAG, "Finish findProviders at " +
                    (System.currentTimeMillis() - start));
        }
    }

    public void addToRouting(@NonNull QueryPeer peer) {
        routingTable.addPeer(peer);
    }

    public void removeFromRouting(QueryPeer peer) {
        boolean result = routingTable.removePeer(peer);
        if (result) {
            LogUtils.info(TAG, "Remove from routing " + peer);
        }
    }


    public void provide(@NonNull Closeable closeable, @NonNull Cid cid) throws InterruptedException {

        if (!cid.isDefined()) {
            LogUtils.debug(TAG, "invalid cid: undefined");
            return;
        }

        bootstrap();

        long start = System.currentTimeMillis();
        byte[] key = cid.getHash();

        List<Multiaddr> addresses = host.listenAddresses();

        if (addresses.isEmpty()) {
            LogUtils.error(TAG, "nothing to do here, no addresses to offer");
            return;
        }

        try {
            Dht.Message.Builder builder = Dht.Message.newBuilder()
                    .setType(Dht.Message.MessageType.ADD_PROVIDER)
                    .setKey(ByteString.copyFrom(key))
                    .setClusterLevelRaw(0);

            Dht.Message.Peer.Builder peerBuilder = Dht.Message.Peer.newBuilder()
                    .setId(ByteString.copyFrom(self.getBytes()));
            for (Multiaddr ma : addresses) {
                peerBuilder.addAddrs(ByteString.copyFrom(ma.getBytes()));
            }
            builder.addProviderPeers(peerBuilder.build());

            Dht.Message message = builder.build();


            Set<PeerId> handled = ConcurrentHashMap.newKeySet();

            getClosestPeers(closeable, key, peers -> {

                List<Peer> notHandled = new ArrayList<>();
                for (Peer peer : peers) {
                    if (peer.hasAddresses()) {
                        if (!handled.contains(peer.getPeerId())) {
                            handled.add(peer.getPeerId());
                            notHandled.add(peer);
                        }
                    }
                }
                for (Peer peer : notHandled) {
                    try {
                        if (!motherfuckers.contains(peer.getPeerId())) {
                            host.connect(host.createSession(), peer.getMultiaddrs(),
                                            IPFS.CONNECT_TIMEOUT, IPFS.CONNECT_TIMEOUT,
                                            0, 20480).
                                    whenComplete((connection, throwable) -> {
                                        if (throwable != null) {
                                            motherfuckers.add(peer.getPeerId());
                                        } else {
                                            try {
                                                sendMessage(connection, message).get(
                                                        IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);
                                                // todo sendMessage will always fail
                                            } catch (Throwable ignore) {
                                                // ignore
                                            } finally {
                                                connection.close();
                                            }
                                        }
                                    });
                        }
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                }

            });
        } finally {
            LogUtils.debug(TAG, "Finish provide at " +
                    (System.currentTimeMillis() - start));
        }

    }

    private CompletableFuture<Void> sendMessage(@NonNull QuicConnection conn, @NonNull Dht.Message message) {
        CompletableFuture<Void> done = new CompletableFuture<>();

        conn.createStream(new StreamDataHandler(new TokenData() {
                    @Override
                    public void throwable(Throwable throwable) {
                        done.completeExceptionally(throwable);
                        conn.close();
                    }

                    @Override
                    public void token(QuicStream stream, String token) throws Exception {
                        if (!Arrays.asList(IPFS.STREAM_PROTOCOL, IPFS.DHT_PROTOCOL).contains(token)) {
                            throw new Exception("Token " + token + " not supported");
                        }
                        if (Objects.equals(token, IPFS.DHT_PROTOCOL)) {
                            stream.writeOutput(DataHandler.encode(message))
                                    .thenApply(QuicStream::closeOutput);
                        }
                    }

                    @Override
                    public void fin() {
                        LogUtils.error(TAG, "fin sendMessage invoked");
                        done.complete(null);
                    }
                }), true, IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS)
                .thenApply(quicStream -> quicStream.writeOutput(
                        DataHandler.writeToken(IPFS.STREAM_PROTOCOL, IPFS.DHT_PROTOCOL)));

        return done;
    }

    private Dht.Message sendRequest(@NonNull QuicConnection conn, @NonNull Peer peer,
                                    @NonNull Dht.Message message)
            throws InterruptedException, ConnectException {


        try {
            AtomicLong latency = new AtomicLong(System.currentTimeMillis());
            CompletableFuture<Dht.Message> done = new CompletableFuture<>();

            conn.createStream(new StreamDataHandler(new StreamData() {
                @Override
                public void throwable(Throwable throwable) {
                    done.completeExceptionally(throwable);
                    conn.close();
                }

                @Override
                public void token(QuicStream stream, String token) throws Exception {
                    if (!Arrays.asList(IPFS.STREAM_PROTOCOL, IPFS.DHT_PROTOCOL).contains(token)) {
                        throw new Exception("Token " + token + " not supported");
                    }
                    if (Objects.equals(token, IPFS.DHT_PROTOCOL)) {
                        stream.writeOutput(DataHandler.encode(message))
                                .thenApply(QuicStream::closeOutput);
                        peer.setLatency(System.currentTimeMillis() - latency.get());
                    }
                }

                @Override
                public void fin() {
                    // nothing to do here
                }

                @Override
                public void data(QuicStream stream, ByteBuffer data) throws Exception {
                    done.complete(Dht.Message.parseFrom(data.array()));
                }

            }), true, IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS).thenApply(quicStream ->
                    quicStream.writeOutput(
                            DataHandler.writeToken(IPFS.STREAM_PROTOCOL, IPFS.DHT_PROTOCOL)));

            Dht.Message msg = done.get(IPFS.DHT_REQUEST_READ_TIMEOUT, TimeUnit.SECONDS);
            Objects.requireNonNull(msg);

            return msg;
        } catch (ExecutionException | TimeoutException exception) {
            LogUtils.debug(TAG, "Request " + conn.getRemoteAddress().toString() + " : " +
                    exception.getClass().getSimpleName() +
                    " : " + exception.getMessage());
            throw new ConnectException(exception.getClass().getSimpleName());

        } finally {
            conn.close();
        }
    }

    private Dht.Message sendRequest(@NonNull Closeable closeable, @NonNull Peer peer,
                                    @NonNull Dht.Message message)
            throws InterruptedException, ConnectException {

        CompletableFuture<Dht.Message> done = new CompletableFuture<>();
        if (closeable.isClosed()) {
            throw new InterruptedException();
        }
        if (motherfuckers.contains(peer.getPeerId())) {
            throw new ConnectException("peer can not be connected too");
        }
        try {
            host.connect(host.createSession(), peer.getMultiaddrs(), IPFS.CONNECT_TIMEOUT,
                            IPFS.CONNECT_TIMEOUT, 0, IPFS.MESSAGE_SIZE_MAX)
                    .whenComplete((connection, throwable) -> {
                        if (throwable != null) {
                            motherfuckers.add(peer.getPeerId());
                            done.completeExceptionally(throwable);
                        } else {
                            try {
                                done.complete(sendRequest(connection, peer, message));
                            } catch (Throwable error) {
                                done.completeExceptionally(error);
                            }
                        }

                    });
            return done.get(IPFS.DHT_REQUEST_READ_TIMEOUT + IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);
        } catch (InterruptedException interruptedException) {
            throw interruptedException;
        } catch (Throwable throwable) {
            throw new ConnectException(throwable.getMessage());
        }
    }


    private Dht.Message getValueSingle(@NonNull Closeable ctx, @NonNull Peer peer, @NonNull byte[] key)
            throws InterruptedException, ConnectException {
        Dht.Message pms = Dht.Message.newBuilder()
                .setType(Dht.Message.MessageType.GET_VALUE)
                .setKey(ByteString.copyFrom(key))
                .setClusterLevelRaw(0).build();
        return sendRequest(ctx, peer, pms);
    }

    private Dht.Message findPeerSingle(@NonNull Closeable ctx, @NonNull Peer p, @NonNull byte[] key)
            throws InterruptedException, ConnectException {
        Dht.Message pms = Dht.Message.newBuilder()
                .setType(Dht.Message.MessageType.FIND_NODE)
                .setKey(ByteString.copyFrom(key))
                .setClusterLevelRaw(0).build();

        return sendRequest(ctx, p, pms);
    }

    private Dht.Message findProvidersSingle(@NonNull Closeable ctx, @NonNull Peer p, @NonNull byte[] key)
            throws InterruptedException, ConnectException {
        Dht.Message pms = Dht.Message.newBuilder()
                .setType(Dht.Message.MessageType.GET_PROVIDERS)
                .setKey(ByteString.copyFrom(key))
                .setClusterLevelRaw(0).build();
        return sendRequest(ctx, p, pms);
    }


    @Nullable
    private Multiaddr createMultiaddr(@NonNull ByteString address) {
        try {
            return Multiaddr.createMultiaddr(address);
        } catch (Throwable ignore) {
            // ignore
        }
        return null;
    }


    public void findPeer(@NonNull Closeable closeable,
                         @NonNull Consumer<Multiaddr> consumer,
                         @NonNull PeerId id) throws InterruptedException {

        bootstrap();

        byte[] key = id.getBytes();
        long start = System.currentTimeMillis();
        try {
            Set<Multiaddr> handled = ConcurrentHashMap.newKeySet();
            runQuery(closeable, key, (ctx, p) -> {

                Dht.Message pms = findPeerSingle(ctx, p, key);

                List<Peer> peers = evalClosestPeers(pms);
                for (Peer peer : peers) {
                    if (Objects.equals(peer.getPeerId(), id)) {
                        for (Multiaddr multiaddr : peer.getMultiaddrs()) {
                            if (!handled.contains(multiaddr)) {
                                handled.add(multiaddr);
                                consumer.accept(multiaddr);
                            }
                        }
                    }
                }

                return peers;

            });
        } finally {
            LogUtils.debug(TAG, "Finish findPeer " + id.toBase58() +
                    " at " + (System.currentTimeMillis() - start));
        }
    }

    private void runQuery(@NonNull Closeable closeable, @NonNull byte[] target,
                          @NonNull QueryFunc queryFn) throws InterruptedException {
        // pick the K closest peers to the key in our Routing table.
        ID key = ID.convertKey(target);
        List<QueryPeer> seedPeers = routingTable.nearestPeers(key);
        if (seedPeers.size() == 0) {
            return;
        }

        Query.runQuery(this, closeable, key, seedPeers, queryFn);

    }

    private List<Peer> getRecordOfPeers(@NonNull Closeable closeable,
                                        @NonNull Peer peer,
                                        @NonNull Consumer<Ipns.Entry> consumer,
                                        @NonNull byte[] key)
            throws InterruptedException, ConnectException {


        Dht.Message pms = getValueSingle(closeable, peer, key);

        List<Peer> peers = evalClosestPeers(pms);

        if (pms.hasRecord()) {

            Dht.Message.Record rec = pms.getRecord();
            try {
                byte[] record = rec.getValue().toByteArray();
                if (record != null && record.length > 0) {
                    Ipns.Entry entry = validator.validate(rec.getKey().toByteArray(), record);
                    consumer.accept(entry);
                }
            } catch (Throwable throwable) {
                peer.setLatency(Long.MAX_VALUE);
                LogUtils.error(TAG, throwable);
            }
        }

        return peers;
    }

    private void getValues(@NonNull Closeable closeable, @NonNull Consumer<Ipns.Entry> consumer,
                           @NonNull byte[] key) throws InterruptedException {
        runQuery(closeable, key, (ctx1, peer) -> getRecordOfPeers(ctx1, peer, consumer, key));
    }


    private void processValues(@Nullable Ipns.Entry best,
                               @NonNull Ipns.Entry current,
                               @NonNull Consumer<Ipns.Entry> reporter) {

        if (best != null) {
            int value = validator.compare(best, current);
            if (value == -1) { // "current" is newer entry
                reporter.accept(current);
            }
        } else {
            reporter.accept(current);
        }
    }


    public void searchValue(@NonNull Closeable closeable, @NonNull Consumer<Ipns.Entry> consumer,
                            @NonNull byte[] key) throws InterruptedException {

        bootstrap();

        AtomicReference<Ipns.Entry> best = new AtomicReference<>();
        long start = System.currentTimeMillis();
        try {
            getValues(closeable, entry -> processValues(best.get(), entry, (current) -> {
                consumer.accept(current);
                best.set(current);
            }), key);
        } finally {
            LogUtils.info(TAG, "Finish searchValue at " +
                    (System.currentTimeMillis() - start));
        }
    }


    public interface QueryFunc {
        @NonNull
        List<Peer> query(@NonNull Closeable closeable, @NonNull Peer peer)
                throws InterruptedException, ConnectException;
    }


}
