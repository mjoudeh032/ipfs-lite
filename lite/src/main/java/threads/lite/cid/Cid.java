package threads.lite.cid;

import androidx.annotation.NonNull;
import androidx.room.TypeConverter;

import com.google.common.io.ByteStreams;

import net.luminis.quic.ImplementationError;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Objects;

public final class Cid implements Comparable<Cid> {

    public static final long IDENTITY = 0x00;
    public static final long Raw = 0x55;
    public static final long DagProtobuf = 0x70;
    public static final long Libp2pKey = 0x72;

    private final byte[] multihash;

    public Cid(byte[] multihash) {
        this.multihash = multihash;
    }

    @TypeConverter
    public static Cid fromArray(byte[] data) {
        if (data == null) {
            return null;
        }
        return new Cid(data);
    }

    @TypeConverter
    public static byte[] toArray(Cid cid) {
        if (cid == null) {
            return null;
        }
        return cid.bytes();
    }


    @NonNull
    public static Cid tryNewCidV0(byte[] mhash) throws IOException {

        Multihash dec = Multihash.deserialize(mhash);

        if (dec.getType().index != Multihash.Type.sha2_256.index
                || Multihash.Type.sha2_256.length != 32) {
            throw new RuntimeException("invalid hash for cidv0");
        }
        return new Cid(dec.toBytes());
    }

    @NonNull
    public static Cid nsToCid(@NonNull String ns) throws IOException, NoSuchAlgorithmException {

        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] hash = digest.digest(ns.getBytes());
        return newCidV1(Raw, hash);

    }

    @NonNull
    public static Cid decode(@NonNull String name) throws IOException {
        if (name.length() < 2) {
            throw new RuntimeException("invalid cid");
        }

        if (name.length() == 46 && name.startsWith("Qm")) {
            Multihash hash = Multihash.fromBase58(name);
            Objects.requireNonNull(hash);
            return new Cid(hash.toBytes());
        }

        byte[] data = Multibase.decode(name);

        try (InputStream inputStream = new ByteArrayInputStream(data)) {
            long version = Multihash.readVarint(inputStream);
            if (version != 1) {
                throw new IOException("invalid version");
            }
            long codecType = Multihash.readVarint(inputStream);
            if (!(codecType == Cid.DagProtobuf || codecType == Cid.Raw || codecType == Cid.Libp2pKey)) {
                throw new IOException("not supported codec");
            }

            Multihash mh = Multihash.deserialize(inputStream);
            Objects.requireNonNull(mh);
            return new Cid(data);

        }
    }

    @NonNull
    public static Cid newCidV0(byte[] mhash) throws IOException {
        return tryNewCidV0(mhash);
    }

    @NonNull
    public static Cid newCidV1(long codecType, byte[] mhash) throws IOException {

        try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            Multihash.putUvarint(out, 1);
            Multihash.putUvarint(out, codecType);
            out.write(mhash);
            return new Cid(out.toByteArray());
        }
    }

    @NonNull
    public static byte[] encode(byte[] buf, long code) throws IOException {

        try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            Multihash.putUvarint(out, code);
            Multihash.putUvarint(out, buf.length);
            out.write(buf);
            return out.toByteArray();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cid cid = (Cid) o;
        return Arrays.equals(multihash, cid.multihash);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(multihash);
    }

    @NonNull
    public String String() {
        switch (getVersion()) {
            case 0:
                try {
                    return Multihash.deserialize(multihash).toBase58();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            case 1:
                return Multibase.encode(Multibase.Base.Base32, multihash);
            default:
                throw new ImplementationError();
        }
    }

    public int getVersion() {
        byte[] bytes = multihash;
        if (bytes.length == 34 && bytes[0] == 18 && bytes[1] == 32) {
            return 0;
        }
        return 1;
    }

    public long getType() {
        if (getVersion() == 0) {
            return DagProtobuf;
        }
        try (InputStream inputStream = new ByteArrayInputStream(multihash)) {
            Multihash.readVarint(inputStream); // ignore first item (version = 1)
            return Multihash.readVarint(inputStream);
        } catch (IOException ioException) {
            throw new RuntimeException(ioException);
        }
    }

    public byte[] bytes() {
        return multihash;
    }

    public boolean isDefined() {
        return multihash != null;
    }

    @NonNull
    public Prefix getPrefix() {

        if (getVersion() == 0) {
            return new Prefix(DagProtobuf, 32, Multihash.Type.sha2_256.index, 0);
        }
        return Prefix.getPrefixFromBytes(bytes());
    }

    @Override
    public int compareTo(Cid o) {
        return Integer.compare(this.hashCode(), o.hashCode());
    }

    public byte[] getHash() {

        if (getVersion() == 0) {
            return multihash;
        } else {
            byte[] data = bytes();
            try {
                try (InputStream inputStream = new ByteArrayInputStream(data)) {
                    long version = Multihash.readVarint(inputStream);
                    if (version != 1) {
                        throw new RuntimeException("invalid version");
                    }
                    long codec = Multihash.readVarint(inputStream);
                    if (!(codec == Cid.DagProtobuf || codec == Cid.Raw || codec == Cid.Libp2pKey)) {
                        throw new RuntimeException("not supported codec");
                    }

                    try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
                        long value = ByteStreams.copy(inputStream, outputStream);
                        if (value <= 0) {
                            throw new RuntimeException("invalid hash");
                        }
                        return outputStream.toByteArray();
                    }
                }
            } catch (Throwable throwable) {
                throw new ImplementationError();
            }
        }
    }

    public boolean isSupported() {
        return getPrefix().isSha2556();
    }
}