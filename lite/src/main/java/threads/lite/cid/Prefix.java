package threads.lite.cid;

import androidx.annotation.NonNull;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class Prefix {

    public final long version;
    public final long codec;
    private final int mhType;
    private final long mhLength;

    public Prefix(long codec, long mhLength, int mhType, long version) {
        this.version = version;
        this.codec = codec;
        this.mhType = mhType;
        this.mhLength = mhLength;
    }

    public static Prefix getPrefixFromBytes(byte[] buf) {

        try (InputStream inputStream = new ByteArrayInputStream(buf)) {
            long version = Multihash.readVarint(inputStream);
            if (version != 1 && version != 0) {
                throw new Exception("invalid version");
            }
            long codec = Multihash.readVarint(inputStream);
            if (!(codec == Cid.DagProtobuf || codec == Cid.Raw || codec == Cid.Libp2pKey)) {
                throw new Exception("not supported codec");
            }

            int mhtype = (int) Multihash.readVarint(inputStream);

            long mhlen = Multihash.readVarint(inputStream);

            return new Prefix(codec, mhlen, mhtype, version);


        } catch (Throwable throwable) {
            throw new RuntimeException(throwable);
        }
    }

    public Multihash.Type getType() {
        return Multihash.Type.lookup(mhType);
    }

    @NonNull
    public Cid sum(byte[] data) throws IOException, NoSuchAlgorithmException {


        if (version == 0 && (!isSha2556()) ||
                (mhLength != 32 && mhLength != -1)) {

            throw new IOException("Invalid v0 Prefix");
        }
        if (!isSha2556()) {
            throw new IOException("Type Multihash " +
                    Multihash.Type.lookup(mhType).name() + " is not supported");
        }


        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] hash = Cid.encode(digest.digest(data), mhType);

        switch ((int) version) {
            case 0:
                return Cid.newCidV0(hash);
            case 1:
                return Cid.newCidV1(codec, hash);
            default:
                throw new IOException("Invalid cid version");
        }


    }

    public boolean isSha2556() {
        return Multihash.Type.lookup(mhType) == Multihash.Type.sha2_256;
    }

    @NonNull
    @Override
    public String toString() {
        return "Prefix{" +
                "version=" + version +
                ", codec=" + codec +
                ", mhType=" + Multihash.Type.lookup(mhType).name() +
                ", mhLength=" + mhLength +
                '}';
    }

    public byte[] bytes() {

        try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            Multihash.putUvarint(out, version);
            Multihash.putUvarint(out, codec);
            Multihash.putUvarint(out, mhType);
            Multihash.putUvarint(out, mhLength);
            return out.toByteArray();
        } catch (Throwable throwable) {
            throw new RuntimeException(throwable);
        }
    }
}
