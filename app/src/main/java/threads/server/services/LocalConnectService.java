package threads.server.services;

import android.content.Context;

import androidx.annotation.NonNull;

import net.luminis.quic.QuicConnection;

import java.net.Inet6Address;
import java.net.InetAddress;

import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Multiaddr;
import threads.lite.host.Session;

public class LocalConnectService {

    private static final String TAG = LocalConnectService.class.getSimpleName();

    public static void connect(@NonNull Session session,
                               @NonNull Context context,
                               @NonNull InetAddress inetAddress,
                               int port) {

        try {
            IPFS ipfs = IPFS.getInstance(context);

            String pre = "/ip4";
            if (inetAddress instanceof Inet6Address) {
                pre = "/ip6";
            }

            String multiAddress = pre + inetAddress + "/udp/" + port + "/quic";
            Multiaddr multiaddr = new Multiaddr(multiAddress);

            if(!session.swarmHas(multiaddr)) {
                QuicConnection conn = ipfs.dial(session, multiaddr, IPFS.CONNECT_TIMEOUT,
                        IPFS.GRACE_PERIOD, IPFS.MAX_STREAMS, IPFS.MESSAGE_SIZE_MAX);
                session.swarmEnhance(conn);

                LogUtils.error(TAG, "Success " + multiAddress);
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

}

